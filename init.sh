# добавляем "plop" в блок "scripts"
sed -i '/"scripts": {/a \   \ "plop": "plop",' "package.json"

# Создаем файл plopfile.mjs
cat << 'EOF' > plopfile.mjs
// eslint-disable-next-line import/extensions
import { plopConfig } from "./templates/index.mjs";

export default function (
  /** @type {import('plop').NodePlopAPI} */
  plop
) {
  plopConfig(plop);
}
EOF

# Сообщаем пользователю о создании файла
echo "Файл plopfile.mjs успешно создан."

yarn add -D plop