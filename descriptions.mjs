export const descriptions = {
  p: "Create new page",
  s: "Create new shared component",
  e: "Create new entity",
  f: "Create new feature",
  w: "Create new widget",
};
