export const camelCase = (str) => {
  return str.split("-").reduce((acc, word, idx) => {
    return `${acc}${idx ? word[0].toUpperCase() : word[0]}${word.slice(1)}`;
  }, "");
};

export const titleCase = (str) => {
  return str.split("-").reduce((acc, word) => {
    return `${acc}${word[0].toUpperCase()}${word.slice(1)}`;
  }, "");
};
