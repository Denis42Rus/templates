/* eslint-disable import/extensions */
import { actions } from "./actions.mjs";
import { descriptions } from "./descriptions.mjs";
import { camelCase, titleCase } from "./helpers.mjs";

const variants = ["p", "s", "e", "f", "w"];

export const plopConfig = (plop) => {
  variants.forEach((item) => {
    plop.setGenerator(item, {
      description: descriptions[item],
      prompts: [
        {
          type: "input",
          name: "name",
          message: "Name:",
        },
      ],
      actions: actions[item],
    });
  });

  plop.setHelper("camelCase", camelCase);
  plop.setHelper("titleCase", titleCase);
};
