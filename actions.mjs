export const actions = {
  p: [
    {
      type: "addMany",
      destination: "src/pages/{{name}}",
      base: "templates/page",
      templateFiles: "templates/page/*.hbs",
    },
  ],
  s: [
    {
      type: "addMany",
      destination: "src/shared/ui/{{name}}",
      base: "templates/shared",
      templateFiles: "templates/shared/*.hbs",
    },
  ],
  e: [
    {
      type: "addMany",
      destination: "src/entities/{{name}}",
      base: "templates/entity",
      templateFiles: "templates/entity/*.hbs",
    },
    {
      type: "addMany",
      destination: "src/entities/{{name}}/ui/{{name}}",
      base: "templates/entity/entity",
      templateFiles: "templates/entity/entity/*.hbs",
    },
    {
      type: "addMany",
      destination: "src/entities/{{name}}/model",
      base: "templates/entity/model",
      templateFiles: "templates/entity/model/*.hbs",
    },
  ],
  f: [
    {
      type: "addMany",
      destination: "src/features/{{name}}",
      base: "templates/feature",
      templateFiles: "templates/feature/*.hbs",
    },
    {
      type: "addMany",
      destination: "src/features/{{name}}/ui/{{name}}",
      base: "templates/feature/feature",
      templateFiles: "templates/feature/feature/*.hbs",
    },
  ],
  w: [
    {
      type: "addMany",
      destination: "src/widgets/{{name}}",
      base: "templates/widget",
      templateFiles: "templates/widget/*.hbs",
    },
    {
      type: "addMany",
      destination: "src/widgets/{{name}}/ui/{{name}}",
      base: "templates/widget/widget",
      templateFiles: "templates/widget/widget/*.hbs",
    },
    {
      type: "addMany",
      destination: "src/widgets/{{name}}/model",
      base: "templates/widget/model",
      templateFiles: "templates/widget/model/*.hbs",
    },
  ],
};
